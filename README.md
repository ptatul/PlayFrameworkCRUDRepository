# PlayFrameworkCRUDRepository
Play Framework Project CRUD with my SQL data with activator starter command.

Admin Utility Application

To generate build file from source code following are the step:-
	
	1) Extract source code from zip provided
	2) cd into project folder with terminal/command prompt
	3) run "./activator" which will download dependencies if not added already from maven repository(one time process)
	4) with play/sbt console, use the command "clean" to clean up the project
	5) use command "compile" to compile the project
	6) use command "run" to run the project in localhost if you will specify -Dhttp.port = any port then it would be run on that specific port.
	7) use command "dist" to generate build file which will usually be added into "{base_Path_To_Project}"/target/universal which will be of .zip extension.
	Note :- All libs for supporting play has been added to project structure which include sbt, activator pluggins to proceed with development work and genration of builds.

To create eclipse project
	1) cd into project folder with terminal/command prompt
	2) run "./activator" which will download dependencies if not added already from maven repository(one time process)
	3) with play/sbt console, use the command "clean" to clean up the project
	4) use command "eclipse" to create eclipse project
	Note :- Project has been pre-configured to work in eclipse IDE. to configure with other IDEs, please refer https://www.playframework.com/documentation/2.6.x/IDE
	
To deploy build into production environment
	1) Unzip folders from snapshot/build file
	2) cd into bin folder with terminal/command prompt
	3) provide executable permission to 'yourProject' if not provided
	4) run script in 'yourProject' file using './yourProject' followed by -Dhttp.port={prefered_port} to start play server.
	Note :- To update existing build, we can replace only the lib and conf directory with the new generated snapshot/build. In this case server has to be stopped and process Id has to be killed if exists.
